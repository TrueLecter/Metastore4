﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using MediatR;
using Metastore.Application.Contracts.Queries.Protected;

using Metastore.WebApi.Extensions;
using Metastore.WebApi.Models.Parameters;
using Metastore.WebApi.Models.Parameters.Filters;

using Metastore.Application.Contracts.Models.Common;
using Metastore.Application.DataFilters;
using Metastore.Application.Contracts.Models;
using Metastore.Domain.StandardTypes;
using FluentValidation;
using Metastore.WebApi.Infrastructure.Extensions;

namespace Metastore.WebApi.Controllers
{
    [RoutePrefix("api/transactions")]

    public class TransactionsController : BaseController
    {

        public TransactionsController(IMediator mediator, IMapper mapper) : base(mediator, mapper) { }

        [ResponseType(typeof(ListPage<TransactionDto>))]
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetAsync(ListPagination pgn = null, ListSorting srt = null, TransactionsApiFilter flt = null)
        {
            var selectParams = (pgn, srt).SelectParamsFor<TransactionDto>();

            var dataFilter = Mapper.Map<TransactionsApiFilter, TransactionsFilter>(flt);

            var query = new TransactionsListQuery(dataFilter, selectParams);

            var list = await Mediator.Send(query);

            var result = Mapper.Map<ListPage<TransactionModel>, ListPage<TransactionDto>>(list);

            return Ok(result);
        }

        #region Mapping

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {

                CreateMap<TransactionModel, TransactionDto>()
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => s.Type.ToString()));

                CreateMap<TransactionsApiFilter, TransactionsFilter>()
                    .ForMember(
                        d => d.Types,
                        opt => opt.ResolveUsing(s => s.Types.ToEnums<TransactionType>()));
            }
        }
        #endregion

        #region DTOs
        public class TransactionDto
        {
            public int Id { get; set; }

            public string Type { get; set; }

            public string FriendlyId { get; set; }
            public decimal Amount { get; set; }
        }

        public class CreateTransactionDto
        {

        }
        #endregion

        #region Validators 
        public class CreateTransactionValidator : AbstractValidator<CreateTransactionDto>
        {

        }
        #endregion
    }
}