﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Results;

using Metastore.WebApi.Extensions;
using Metastore.WebApi.Results;
using Metastore.Application.Contracts.Models.Common;
using MediatR;
using AutoMapper;

namespace Metastore.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        protected readonly IMediator Mediator;

        protected readonly IMapper Mapper;

        public BaseController(IMediator mediator, IMapper mapper)
        {
            Mediator = mediator;
            Mapper = mapper;
        }

        protected virtual OkListPageResult<T> Ok<T>(ListPage<T> content)
        {
            return this.OkListPageResult(content);
        }

        #region Common DTOs
        // Define commong DTOs here
        #endregion

        #region Common validators
        // Define commong validators here
        #endregion

        public class CommonMappingProfile : Profile
        {
            public CommonMappingProfile()
            {
                // Define common model-to-DTO mappings here

                CreateMap(typeof(ListPage<>), typeof(ListPage<>)).ConvertUsing(typeof(ListPageConverter<,>));
            }
        }



        public class ListPageConverter<TSourceElem, TDestElem> : ITypeConverter<ListPage<TSourceElem>, ListPage<TDestElem>>
        {
            public ListPage<TDestElem> Convert(ListPage<TSourceElem> source, ListPage<TDestElem> destination, ResolutionContext context)
            => new ListPage<TDestElem>(context.Mapper.Map<ICollection<TSourceElem>, ICollection<TDestElem>>(source),
            source.TotalCount);
        }
    }
}