﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using MediatR;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.StoreCommands;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Application.DataFilters;
using Metastore.Domain.StandardTypes;
using Metastore.WebApi.Extensions;
using Metastore.WebApi.Models.Parameters;

namespace Metastore.WebApi.Controllers
{
    [RoutePrefix("api/stores")]
    public class StoresController : BaseController
    {
        public StoresController(IMediator mediator, IMapper mapper) : base(mediator, mapper) { }

        //GET api/stores
        [ResponseType(typeof(ListPage<StoreDto>))]
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetStoresAsync(ListPagination pgn = null, ListSorting srt = null, StoresApiFilter flt = null)
        {
            var selectParams = (pgn, srt).SelectParamsFor<StoreDto>();
            var dataFilter = Mapper.Map<StoresApiFilter, StoresFilter>(flt);

            var query = new StoresListQuery(dataFilter, selectParams);

            var list = await Mediator.Send(query);

            var result = Mapper.Map<ListPage<StoreModel>, ListPage<StoreDto>>(list);

            return Ok(result);
        }


        // GET api/stores/{Store_id}
        [ResponseType(typeof(SingleStoreDto))]
        [HttpGet]
        public async Task<IHttpActionResult> GetStoreInfo(int id)
        {
            var query = new StoreByIdQuery(id);

            var res1 = await Mediator.Send(query);

            var res = Mapper.Map<StoreModel, SingleStoreDto>(res1);

            return Ok(res);
        }

        // POST api/Stores/create
        [HttpPost]
        [Route("create")]
        public async Task<IHttpActionResult> CreateStoreAsync([FromBody] CreateStoreDto newStore)
        {
            var store = Mapper.Map<CreateStoreDto, StoreModel>(newStore);
            var command = new CreateStoreCommand(store);
            var result = await Mediator.Send(command);

            return Ok(result);
        }

        // DELETE api/Stores/delete
        [HttpDelete]
        [Route("delete")]
        public async Task<IHttpActionResult> DeleteStoreAsync(int storeId)
        {
            var command = new DeleteStoreCommand(storeId);

            var result = await Mediator.Send(command);

            return Ok(result);
        }

        // PUT api/Stores/edit
        [HttpPut]
        [Route("edit")]
        public async Task<IHttpActionResult> EditStoreAsync([FromBody] StoreDto newStore)
        {
            var store = Mapper.Map<StoreDto, StoreModel>(newStore);
            var command = new EditStoreCommand(store);
            var result = await Mediator.Send(command);

            return Ok(result);

        }


        #region DTOs
        public class StoreDto
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string Country { get; set; }
            
            public string Currency { get; set; }  
        }

        public class SingleStoreDto
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string Country { get; set; }

            public string Currency { get; set; }

            public List<ItemsController.ItemDto> Items { get; set; }
        }

        public class CreateStoreDto
        {
            public string Name { get; set; }

            public string Country { get; set; }

            public string Currency { get; set; }
        }

        #endregion

        #region Mapping

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {

                CreateMap<StoreDto, StoreModel>()
                    .ForMember(dst => dst.Currency, opt => opt.MapFrom(s => Enum.Parse(typeof(Currency), s.Currency)));

                CreateMap<CreateStoreDto, StoreModel>();

                CreateMap<StoreModel, StoreDto>();

                CreateMap<StoreModel, SingleStoreDto>();

                CreateMap<StoresApiFilter, StoresFilter>();

            }
        }
        #endregion
    }
}
