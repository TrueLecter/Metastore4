﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using MediatR;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Application.DataFilters;
using Metastore.WebApi.Models.Parameters.Filters;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.WishlistCommands;
using Metastore.Application.Contracts.Results;
using Microsoft.AspNet.Identity;

namespace Metastore.WebApi.Controllers
{
    //[Authorize]
    [RoutePrefix("api/user")]
    public class UserController : BaseController
    {
        public UserController(IMediator mediator, IMapper mapper) : base(mediator, mapper)
        {
        }

        // GET api/user/wishlist
        [ResponseType(typeof(WishlistDto))]
        [Route("wishlist")]
        [HttpGet]
        public async Task<IHttpActionResult> GetWishlist()
        {
            var userId = User.Identity.GetUserId();

            var query = new WishlistQuery(userId);

            var res1 = await Mediator.Send(query);

            var list = Mapper.Map<List<WishlistItemModel>, List<WishlistItemDto>>(res1);

            var res = new WishlistDto { UserId = userId, Items = list};

            return Ok(res);
        }

        // POST api/user/wishlist
        [ResponseType(typeof(CreateResult<bool>))]
        [Route("wishlist")]
        [HttpPost]
        public async Task<IHttpActionResult> AddItemToWishlist([FromBody]int itemId)
        {
            var userId = User.Identity.GetUserId();
            var query = new AddItemToWishlistCommand(userId, itemId);

            var res = await Mediator.Send(query);

            return Ok(res);
        }

        // DELETE api/user/wishlist
        [ResponseType(typeof(DeleteResult))]
        [Route("wishlist")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveItemFromWishlist([FromBody]int itemId)
        {
            var userId = User.Identity.GetUserId();

            var query = new DeleteItemFromWishlistCommand(userId, itemId);

            var res = await Mediator.Send(query);

            return Ok(res);
        }

        #region DTO's

        public class WishlistItemDto
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string Seller { get; set; }

            public double Price { get; set; }

            public bool Aviable { get; set; }

            public DateTime DateAdded { get; set; }

        }

        public class WishlistDto
        {
            public string UserId { get; set; }

            public List<WishlistItemDto> Items { get; set; }
        }

        #endregion

        #region Mapping
        public class MappingProfile : Profile
        {
            public MappingProfile()
            {

                CreateMap<WishlistItemDto, WishlistItemModel>();

                CreateMap<WishlistItemModel, WishlistItemDto>();
                
                CreateMap<WishlistItemsApiFilter, WishlistItemFilter>();

            }
        }


        #endregion
    }
}
