﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using MediatR;

namespace Metastore.WebApi.Controllers
{
    [Authorize]
    public class AuthorizedController : BaseController
    {
        public AuthorizedController(IMediator mediator, IMapper mapper) : base(mediator, mapper)
        {
        }
    }
}