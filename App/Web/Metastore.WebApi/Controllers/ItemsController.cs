﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using MediatR;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.ItemCommands;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Application.DataFilters;
using Metastore.Domain.StandardTypes;
using Metastore.WebApi.Extensions;
using Metastore.WebApi.Models.Parameters;

namespace Metastore.WebApi.Controllers
{
    [RoutePrefix("api/items")]
    public class ItemsController : BaseController
    {
        public ItemsController(IMediator mediator, IMapper mapper) : base(mediator, mapper) { }

        [ResponseType(typeof(ListPage<ItemDto>))]
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetItemsAsync(ListPagination pgn = null, ListSorting srt = null, ItemsApiFilter flt = null)
        {
            var selectParams = (pgn, srt).SelectParamsFor<ItemDto>();

            var dataFilter = Mapper.Map<ItemsApiFilter, ItemsFilter>(flt);

            var query = new ItemsListQuery(dataFilter, selectParams);

            var list = await Mediator.Send(query);

            var result = Mapper.Map<ListPage<ItemModel>, ListPage<ItemDto>>(list);

            return Ok(result);
        }

        // GET api/items/{item_id}
        [ResponseType(typeof(SingleItemDto))]
        [HttpGet]
        public async Task<IHttpActionResult> GetItemInfo(int id)
        {
            //var selectParams = SelectParamsExtensions.BuildForField("Id");

            var query = new ItemByIdQuery(id);

            var res1 = await Mediator.Send(query);

            var res = Mapper.Map<ItemModel,SingleItemDto>(res1);

            return Ok(res);
        }

        // POST api/items/create
        [HttpPost]
        [Route("create")]
        public async Task<IHttpActionResult> CreateItemAsync([FromBody] CreateItemDto newItem)
        {
            var item = Mapper.Map<CreateItemDto, ItemModel>(newItem);
            var command = new CreateItemCommand(item);
            var result = await Mediator.Send(command);

            return Ok(result);
        }

        // DELETE api/items/delete
        [HttpDelete]
        [Route("delete")]
        public async Task<IHttpActionResult> DeleteItemAsync(int itemId)
        {
            var command = new DeleteItemCommand(itemId);

            var result = await Mediator.Send(command);
            
            return Ok(result);
        }

        // PUT api/items/edit
        [HttpPut]
        [Route("edit")]
        public async Task<IHttpActionResult> EditItemAsync([FromBody] CreateItemDto newItem)
        {
            var item = Mapper.Map<CreateItemDto, ItemModel>(newItem);
            var command = new EditItemCommand(item);
            var result = await Mediator.Send(command);

            return Ok(result);

        }

        #region DTOs
        public class SingleItemDto
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string Description { get; set; }

            public DateTime CreatedOn { get; set; }

            public string CreatedBy { get; set; }

            public string SellerName { get; set; }

            public double Price { get; set; }

            public string Currency { get; set; }

            public int Quantity { get; set; }

            //Images URI
            public string[] Images { get; set; }

            public string Thumbnail { get; set; }

            public bool Enabled { get; set; }

            public string Type { get; set; }

            public double Width { get; set; }

            public double Height { get; set; }

            public double Weight { get; set; }

            //File URI
            public string DigitalFile { get; set; }

            public string[] Tags { get; set; }

            public int StoreId { get; set; }
        }

        public class CreateItemDto
        {

            public string Name { get; set; }

            public string Description { get; set; }

            public DateTime CreatedOn { get; set; }

            public string CreatedBy { get; set; }

            public string SellerName { get; set; }

            public double Price { get; set; }

            public string Currency { get; set; }

            public int Quantity { get; set; }

            //Images URI
            public string[] Images { get; set; }

            public string Thumbnail { get; set; }

            public bool Enabled { get; set; }

            public string Type { get; set; }

            public double Width { get; set; }

            public double Height { get; set; }

            public double Weight { get; set; }

            //File URI
            public string DigitalFile { get; set; }

            public string[] Tags { get; set; }

            public int StoreId { get; set; }
        }

        public class ItemDto
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string Description { get; set; }

            public string CreatedBy { get; set; }

            public double Price { get; set; }

            public string Currency { get; set; }

            public int Quantity { get; set; }

            //Images URI
            
            public string Thumbnail { get; set; }

            public bool Enabled { get; set; }

            public string Type { get; set; }
        }

        #endregion

        #region Mapping

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {

                CreateMap<CreateItemDto, ItemModel>()
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => Enum.Parse(typeof(ItemType), s.Type)))
                    .ForMember(d => d.Currency, opt => opt.MapFrom(s => Enum.Parse(typeof(Currency), s.Currency)))

                    .ForMember(d => d.Tags, opt => opt.MapFrom(s => String.Join("|", s.Type)))
                    .ForMember(d => d.Images, opt => opt.MapFrom(s => String.Join("|", s.Images)));

                CreateMap<ItemModel, SingleItemDto>()
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => s.Type.ToString()))
                    .ForMember(d => d.Currency, opt => opt.MapFrom(s => s.Currency.ToString()))

                    .ForMember(d => d.Tags, opt => opt.MapFrom(s => s.Tags.Split('|')))
                    .ForMember(d => d.Images, opt => opt.MapFrom(s => s.Images.Split('|')));


                CreateMap<ItemModel, ItemDto>()
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => s.Type.ToString()))
                    .ForMember(d => d.Currency, opt => opt.MapFrom(s => s.Currency.ToString()));

                CreateMap<ItemsApiFilter, ItemsFilter>()
                .ForMember(
                    d => d.Types,
                    opt => opt.ResolveUsing(s => s.Types.Select(t => Enum.Parse(typeof(ItemType), t)).ToList()));
                   
            }
        }
        #endregion

    }
}
