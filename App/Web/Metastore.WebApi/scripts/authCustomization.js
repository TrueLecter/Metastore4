﻿function addApiKeyAuthorization() {
    var key = encodeURIComponent($('#input_apiKey')[0].value);
    if (key && key.trim() != "") {

        var tokenAuth = new SwaggerClient.ApiKeyAuthorization("Authorization", "Bearer " + key, "header");
        window.swaggerUi.api.clientAuthorizations.add("my_token", tokenAuth);
        log("added key " + key);
    }
}