﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metastore.WebApi.Infrastructure.Extensions
{
    public static class HandyExtensions
    {
        public static IList<T> ToEnums<T>(this ICollection<string> stringConstants) => stringConstants
            .Select(t => Enum.Parse(typeof(T), t, ignoreCase: true)).Cast<T>().ToList();
    }
}