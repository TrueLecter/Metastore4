﻿
using FluentValidation.Validators;
using System;

namespace Metastore.WebApi.Infrastructure.Validation
{
    public class IsStringInEnum<TEnum> : PropertyValidator where TEnum : struct
    {
        public IsStringInEnum(string errorMessage) : base(errorMessage) { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var stringValue = context.PropertyValue as string;

            if (string.IsNullOrWhiteSpace(stringValue))
                return false;

            TEnum result;
            return Enum.TryParse(stringValue, out result);
        }
    }
}