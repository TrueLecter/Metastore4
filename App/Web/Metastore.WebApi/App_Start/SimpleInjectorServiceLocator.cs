using System;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using SimpleInjector;

namespace Metastore.WebApi
{
    public class SimpleInjectorServiceLocator: IServiceLocator
    {
        private Container _container;

        public SimpleInjectorServiceLocator(Container container)
        {
            _container = container;
        }
        public object GetService(Type serviceType)
        {
            return _container.GetInstance(serviceType);
        }

        public object GetInstance(Type serviceType)
        {
            return GetService(serviceType);
        }

        public object GetInstance(Type serviceType, string key)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.GetAllInstances(serviceType);
        }

        public TService GetInstance<TService>()
        {
            return (TService) _container.GetInstance(typeof(TService));
        }

        public TService GetInstance<TService>(string key)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetAllInstances<TService>()
        {
            throw new NotImplementedException();
        }

        IEnumerable<TService> IServiceLocator.GetAllInstances<TService>()
        {
            throw new NotImplementedException();
        }
    }
}