using System.Web.Http;
using System.Web.Http.Cors;

using Newtonsoft.Json.Serialization;
using SimpleInjector.Integration.WebApi;
using WebApi.OutputCache.Core.Cache;
using WebApi.OutputCache.V2;

namespace Metastore.WebApi
{
    public static class WebApiConfig
    {
        public static HttpConfiguration Register()
        {
            var config = new HttpConfiguration();

            // Web API configuration and services
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver =
                new CamelCasePropertyNamesContractResolver();

#if DEBUG
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
#endif
            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional}
            );

            // caching configuration
            var cacheConfig = config.CacheOutputConfiguration();
            cacheConfig.RegisterCacheOutputProvider(() => new MemoryCacheDefault());
            
            return config;
        }
    }
}