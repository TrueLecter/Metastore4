﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Metastore.Infrastructure.Azure;
using MediatR;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.ServiceLocation;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;

using Metastore.Data.Contracts;
using FluentValidation;
using Metastore.Application.Contracts.Queries.Protected;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SimpleInjector.Advanced;
using static Metastore.WebApi.Controllers.TransactionsController;
using Metastore.WebApi.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Metastore.Data.Contexts;
using Metastore.Identity;
using Metastore.WebApi.Controllers;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Metastore.Application.CommandHandlers;
using Metastore.Application.CommandHandlers.ItemCommandHandlers;
using Metastore.Application.CommandHandlers.StoreCommandHandlers;
using Metastore.Application.CommandHandlers.WishlistCommandHandlers;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.ItemCommands;
using Metastore.Application.Contracts.Commands.Protected.StoreCommands;
using Metastore.Application.Contracts.Commands.Protected.WishlistCommands;
using Metastore.Application.Cqrs.QueryHandlers;
using Metastore.Application.QueryHandlers;
using Metastore.Application.QueryHandlers.ItemQueryHandlers;
using Metastore.Application.QueryHandlers.StoreQueryHandlers;
using Metastore.Data.Stores;
using Owin;
using SimpleInjector.Extensions.ExecutionContextScoping;
using SimpleInjector.Lifestyles;

namespace Metastore.WebApi
{
    public static class SimpleInjectorConfig
    {
        public static void UseOwinContextInjector(this IAppBuilder app, Container container)
        {
            // Create an OWIN middleware to create an execution context scope
            app.Use(async (context, next) => {
                using (AsyncScopedLifestyle.BeginScope(container))
                {
                    await next();
                }
            });
        }

        public static void Register(this Container container, HttpConfiguration config)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();


            RegisterInfrastructure(container);

            RegisterDataInfrastructure(container);

            RegisterMapping(container);

            RegisterUserManagement(container);

            // This is an extension method from the SimpleInjector integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            ServiceLocator.SetLocatorProvider(() => new SimpleInjectorServiceLocator(container));
            
        }


        static void RegisterInfrastructure(Container container)
        {
            // MediatR setup
            container.RegisterSingleton<IMediator, Mediator>();
            container.RegisterSingleton(new SingleInstanceFactory(container.GetInstance));
            container.RegisterSingleton(new MultiInstanceFactory(container.GetAllInstances));
            container.RegisterCollection(typeof(IPipelineBehavior<,>), new[] { typeof(IMediator).Assembly });

            var assemblies = GetCqrsAssemblies().ToArray();
            container.Register(typeof(IRequestHandler<,>), assemblies);
            container.Register(typeof(IAsyncRequestHandler<,>), assemblies);

            // Validators
            container.Register(typeof(IValidator<>), new[] { typeof(CreateTransactionValidator).Assembly });

            //Data queries
            container.Register(typeof(IDataView<>), GetDataViewsAssemblies());

            // Storage
            //container.Register<IAzureStorage, AzureStorage>(Lifestyle.Singleton);
        }

        static void RegisterDataInfrastructure(Container container)
        {
            container.Register<IDbExecutionStrategy>(() => new SqlAzureExecutionStrategy(5, TimeSpan.FromMilliseconds(100)), Lifestyle.Singleton);

            container.Register<StoresDataContext>(Lifestyle.Scoped);
        }

        static void RegisterUserManagement(Container container)
        {
            container.Register<IdentityDbContext<ApplicationUser>, AuthDbContext>(Lifestyle.Scoped);

            container.Register(() =>
                container.IsVerifying() ?
                    new ApplicationUserManager(new UserStore<ApplicationUser>())
                    : ApplicationUserManager.Create(container.GetInstance<IdentityDbContext<ApplicationUser>>(), container.GetInstance<IDataProtector>()), Lifestyle.Scoped);

            container.Register<ITextEncoder, Base64TextEncoder>();
            container.Register<IDataSerializer<AuthenticationTicket>, TicketSerializer>();
            container.Register(() => new DpapiDataProtectionProvider().Create("ASP.NET Identity"));

            container.Register(typeof(ISecureDataFormat<>), typeof(SecureDataFormat<>));
        }

        static void RegisterMapping(Container container)
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfiles(GetMappingProfileAssemblies()));
            container.Register(() => config.CreateMapper());
        }



        private static IEnumerable<Assembly> GetCqrsAssemblies()
        {
            yield return typeof(IMediator).GetTypeInfo().Assembly;
            yield return typeof(TransactionsListQuery).GetTypeInfo().Assembly;
            yield return typeof(ItemsListQuery).GetTypeInfo().Assembly;
            yield return typeof(ItemByIdQuery).GetTypeInfo().Assembly;
            yield return typeof(WishlistQuery).GetTypeInfo().Assembly;
            yield return typeof(StoresListQuery).GetTypeInfo().Assembly;
            yield return typeof(StoreByIdQuery).GetTypeInfo().Assembly;

            yield return typeof(CreateItemCommand).GetTypeInfo().Assembly;
            yield return typeof(EditItemCommand).GetTypeInfo().Assembly;
            yield return typeof(DeleteItemCommand).GetTypeInfo().Assembly;
            yield return typeof(CreateStoreCommand).GetTypeInfo().Assembly;
            yield return typeof(EditStoreCommand).GetTypeInfo().Assembly;
            yield return typeof(DeleteStoreCommand).GetTypeInfo().Assembly;
            yield return typeof(AddItemToWishlistCommand).GetTypeInfo().Assembly;
            yield return typeof(DeleteItemFromWishlistCommand).GetTypeInfo().Assembly;

            yield return typeof(TransactionsQueriesHandler).GetTypeInfo().Assembly;
            yield return typeof(ItemsQueriesHandler).GetTypeInfo().Assembly;
            yield return typeof(CreateItemCommandHandler).GetTypeInfo().Assembly;
            yield return typeof(EditItemCommandHandler).GetTypeInfo().Assembly;
            yield return typeof(DeleteItemCommandHandler).GetTypeInfo().Assembly;
            yield return typeof(AddItemToWishlistCommandHandler).GetTypeInfo().Assembly;
            yield return typeof(DeleteItemFromWishlistCommandHandler).GetTypeInfo().Assembly;
        }


        private static IEnumerable<Assembly> GetDataViewsAssemblies()
        {
            yield return typeof(TransactionsDataView).Assembly;
        }

        private static IEnumerable<Assembly> GetMappingProfileAssemblies()
        {
            // add more required assemblies here
            yield return typeof(TransactionsController).GetTypeInfo().Assembly; // Controller assembly (de facto WebApi assembly)
            yield return typeof(ItemsController).GetTypeInfo().Assembly; // Controller assembly (de facto WebApi assembly)
            yield return typeof(StoresController).GetTypeInfo().Assembly; // Controller assembly (de facto WebApi assembly)

            yield return typeof(TransactionsQueriesHandler).GetTypeInfo().Assembly; // CQRS assembly
            yield return typeof(CreateItemCommandHandler).GetTypeInfo().Assembly; // CQRS assembly
            yield return typeof(CreateStoreCommandHandler).GetTypeInfo().Assembly; // CQRS assembly
            yield return typeof(ItemsQueriesHandler).GetTypeInfo().Assembly; // CQRS assembly
            yield return typeof(StoresQueriesHandler).GetTypeInfo().Assembly; // CQRS assembly
            yield return typeof(WishlistQueriesHandler).GetTypeInfo().Assembly; // CQRS assembly
        }

    }
}