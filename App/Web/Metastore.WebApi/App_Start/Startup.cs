﻿using Owin;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;

namespace Metastore.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var httpConfig = WebApiConfig.Register();

            var container = new Container();
            container.Register(httpConfig);

            app.UseOwinContextInjector(container);

            httpConfig.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            
            SwaggerConfig.Register(httpConfig);
            
            ConfigureAuth(app, httpConfig);

            app.UseWebApi(httpConfig);
        }
    }
}