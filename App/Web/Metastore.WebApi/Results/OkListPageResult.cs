﻿using Metastore.Application.Contracts.Models.Common;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Metastore.WebApi.Results
{
    public class OkListPageResult<TElem> : OkNegotiatedContentResult<ListPage<TElem>>
    {

        public OkListPageResult(ListPage<TElem> content, IContentNegotiator contentNegotiator, HttpRequestMessage request, IEnumerable<MediaTypeFormatter> formatters) : base(content, contentNegotiator, request, formatters)
        {
        }

        public OkListPageResult(ListPage<TElem> content, ApiController controller) : base(content, controller)
        {
        }

        public override async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var result = await base.ExecuteAsync(cancellationToken);

            result.Headers.Add(
                "Access-Control-Expose-Headers", "X-Collection-Total"
                );
            
            result.Headers.Add("X-Collection-Total", Content.TotalCount.ToString());

            return result;
        }
 
    }
}