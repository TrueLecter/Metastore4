﻿using System.ComponentModel;
using Metastore.WebApi.ModelBinding;
using System;
using System.Linq;

namespace Metastore.WebApi.Controllers
{
    [TypeConverter(typeof(FilterConverter<StoresApiFilter>))]
    public class StoresApiFilter
    {
        public string[] Types { get; set; }

        public static bool TryParse(string value, out StoresApiFilter filter)
        {
            try
            {
                var fltDict = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToDictionary(fkey => fkey.Split(':')[0], fval => fval.Split(':')[1]);

                filter = new StoresApiFilter
                {
                    Types = fltDict.ContainsKey("type")
                        ? fltDict["type"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        : new string[0],
                };
                return true;
            }
            catch
            {
                filter = new StoresApiFilter
                {
                    Types = new string[0]
                };
                return false;
            }
        }
    }
}