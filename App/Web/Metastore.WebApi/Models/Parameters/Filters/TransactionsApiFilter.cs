using System;
using System.ComponentModel;
using System.Linq;
using Metastore.WebApi.ModelBinding;

namespace Metastore.WebApi.Models.Parameters.Filters
{
    [TypeConverter(typeof(FilterConverter<TransactionsApiFilter>))]
    public class TransactionsApiFilter
    {
        public string[] Types { get; set; }

        public static bool TryParse(string value, out TransactionsApiFilter filter)
        {
            try
            {
                var fltDict = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToDictionary(fkey => fkey.Split(':')[0], fval => fval.Split(':')[1]);

                filter = new TransactionsApiFilter
                {
                    Types = fltDict.ContainsKey("type")
                        ? fltDict["type"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        : new string[0],
                };
                return true;
            }
            catch
            {
                filter = new TransactionsApiFilter
                {
                    Types = new string[0]
                };
                return false;
            }
        }
    }
}