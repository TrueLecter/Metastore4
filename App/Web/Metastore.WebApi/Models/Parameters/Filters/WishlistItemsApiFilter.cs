﻿using System;
using System.ComponentModel;
using System.Linq;
using Metastore.WebApi.ModelBinding;

namespace Metastore.WebApi.Models.Parameters.Filters
{
    [TypeConverter(typeof(FilterConverter<WishlistItemsApiFilter>))]
    public class WishlistItemsApiFilter
    {
        public string[] Types { get; set; }

        public static bool TryParse(string value, out WishlistItemsApiFilter filter)
        {
            try
            {
                var fltDict = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToDictionary(fkey => fkey.Split(':')[0], fval => fval.Split(':')[1]);

                filter = new WishlistItemsApiFilter
                {
                    Types = fltDict.ContainsKey("date")
                        ? fltDict["type"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        : new string[0],
                };
                return true;
            }
            catch
            {
                filter = new WishlistItemsApiFilter
                {
                    Types = new string[0]
                };
                return false;
            }
        }
    }
}