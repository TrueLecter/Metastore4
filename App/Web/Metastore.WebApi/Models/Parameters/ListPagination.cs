﻿using System.ComponentModel;
using Metastore.WebApi.ModelBinding;

namespace Metastore.WebApi.Models.Parameters
{
    [TypeConverter(typeof(PaginationConverter))]
    public class ListPagination
    {
        public int Skip { get; }
        public int Take { get; }

        public ListPagination(int skip, int take)
        {
            Skip = skip;
            Take = take;
        }

        public static bool TryParse(string s, out ListPagination result)
        {
            result = null;

            var parts = s.Split(',');
            if (parts.Length != 2)
            {
                return false;
            }

            int skip, take;
            if (int.TryParse(parts[0], out skip) &&
                int.TryParse(parts[1], out take))
            {
                result = new ListPagination(skip, take);
                return true;
            }
            return false;
        }
    }
}