﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Metastore.WebApi.Extensions;

namespace Metastore.WebApi.Models.Parameters
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SortingParamsAttribute : Attribute
    {
        public SortingParamsAttribute(string fieldName, bool isDefault = false)
        {
            Field = fieldName;
            Default = isDefault;
        }

        public string Field { get; private set; }
        public bool Default { get; private set; }

        public static string FromProperty<TSource, TProperty>(Expression<Func<TSource, TProperty>> propertyLambda)
        {
            return (propertyLambda.Body as MemberExpression)?.GetAttribute<SortingParamsAttribute>()?.Field;
        }

        public static string FromObject(Type objectType, string propertyName, bool withDefault = true)
        {
            if (!withDefault && string.IsNullOrEmpty(propertyName)) return null;

            var sortableProperties = objectType.GetProperties().Select(p => Tuple.Create(p, p.GetCustomAttributes(false).OfType<SortingParamsAttribute>().FirstOrDefault())).ToList();

            if (!sortableProperties.Any())
                return string.Empty;
            
            var sortableProperty =
                sortableProperties.FirstOrDefault(
                    sp => sp.Item1.Name.ToLower() == propertyName?.ToLower() || (propertyName==null && sp.Item2 != null && sp.Item2.Default));

            return sortableProperty?.Item2?.Field ??
                   sortableProperty?.Item1.Name ??
                   sortableProperties.First().Item2?.Field ??
                   sortableProperties.First().Item1.Name;

        }
    }
}