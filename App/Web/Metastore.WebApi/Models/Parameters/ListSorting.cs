﻿using System.ComponentModel;
using Metastore.WebApi.ModelBinding;

namespace Metastore.WebApi.Models.Parameters
{
    [TypeConverter(typeof(SortingConverter))]
    public class ListSorting
    {
        public string FieldName { get; }
        public string Direction { get; }

        public ListSorting(string fldName, string direction)
        {
            FieldName = fldName;
            Direction = direction;
        }

        public static bool TryParse(string s, out ListSorting result)
        {
            result = null;

            var parts = s.Split(',');
            if (parts.Length != 2)
            {
                return false;
            }


            result = new ListSorting(parts[0], parts[1]);
            return true;

        }
    }
}