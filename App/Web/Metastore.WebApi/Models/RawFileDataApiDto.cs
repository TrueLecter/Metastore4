﻿namespace Metastore.WebApi.Models
{
    public class RawFileDataApiDto
    {
        public byte[] Data { get; internal set; }
        public string FileName { get; internal set; }
        public string MimeType { get; internal set; }
    }
}