﻿using Metastore.WebApi;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Metastore.WebApi
{
    public partial class Startup {}
}