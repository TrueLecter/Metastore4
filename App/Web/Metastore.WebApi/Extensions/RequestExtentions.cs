﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using Metastore.WebApi.Models;

namespace Metastore.WebApi.Extensions
{
    public static class RequestExtentions
    {
        public static async Task<IEnumerable<RawFileDataApiDto>> ToFiles(this HttpContent httpContent)
        {
            var multipart = await httpContent.ReadAsMultipartAsync(new MultipartMemoryStreamProvider());

            var files = new List<RawFileDataApiDto>();

            foreach (var content in multipart.Contents)
            {
                files.Add(new RawFileDataApiDto()
                {
                    Data = await content.ReadAsByteArrayAsync(),
                    FileName = Path.GetFileName(content.Headers.ContentDisposition.FileName.Replace("\"", "")),
                    MimeType = content.Headers.ContentType.MediaType
                });
            }

            return files;
        }

        public static async Task<RawFileDataApiDto> ToFile(this HttpContent content) => (await content.ToFiles())
            .FirstOrDefault();
    }
}
