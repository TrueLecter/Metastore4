﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

using Metastore.WebApi.Results;
using Microsoft.AspNet.Identity;
using Metastore.Application.Contracts.Models.Common;

namespace Metastore.WebApi.Extensions
{
    public static class ResultExtensions
    {
        public static OkListPageResult<TElem> OkListPageResult<TElem>(this ApiController controller,
            ListPage<TElem> content)
        {
            return new OkListPageResult<TElem>(content, controller);
        }
    }
}