﻿using System;
using Metastore.Data.Contracts;
using Metastore.WebApi.Models.Parameters;

namespace Metastore.WebApi.Extensions
{
    public static class SelectParamsExtensions
    {
        public static SelectParams ToSelectParams<T>(this ListPagination pgn)
            => SelectParamsFor<T>((pgn,null));



        public static SelectParams SelectParamsFor<T>(this (ListPagination, ListSorting) apiParams)
        {
            var (pgn, srt) = apiParams;

            if (pgn == null && srt == null)
                return null;

            var sortField = SortingParamsAttribute.FromObject(typeof(T), srt?.FieldName);

            return new SelectParams()
            {
                Skip = pgn?.Skip ?? 0,
                Take = pgn?.Take,
                SortField = sortField,
                SortDesc = srt?.Direction.ToLower() == "desc"
            };
        }
        public static SelectParams BuildForField(string sortField)
        {
            SelectParams prm = null;
            if (!string.IsNullOrEmpty(sortField))
            {
                prm = new SelectParams()
                {
                    SortField = sortField
                };
            }
            return prm;
        }


    }
}