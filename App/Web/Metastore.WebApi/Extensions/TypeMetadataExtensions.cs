﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Metastore.WebApi.Extensions
{
    public static class TypeMetadataExtensions
    {
        public static TAttr GetAttribute<TAttr>(this MemberExpression memberExpresison) where TAttr : Attribute
        {
            return (TAttr)memberExpresison.Member.GetCustomAttributes(typeof(TAttr), false).FirstOrDefault();
        }
    }
}