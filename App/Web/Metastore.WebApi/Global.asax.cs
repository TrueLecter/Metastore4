﻿using System;
using System.Web;
using Metastore.WebApi.Filters;
using Microsoft.ApplicationInsights;

namespace Metastore.WebApi
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start() {}

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Response.Filter = new CaptureStream(Response.Filter);
        }

        protected void Application_LogRequest(object sender, EventArgs e)
        {
            // skip requests with failed response
            if (Response.StatusCode >= 500)
                return;

            if (Response.Filter is CaptureStream)
            {
                var len = Response.Filter.Length;
                var trackBody = len >= 0 && len < 8192;

                var requestTelemetry = Context.GetRequestTelemetry();
                var telemetryClient = new TelemetryClient();

                if (trackBody)
                {
                    var captureStreamFilter = Response.Filter as CaptureStream;
                    requestTelemetry.Properties.Add("ResponseBody", captureStreamFilter.StreamAsString);
                }

                telemetryClient.TrackRequest(requestTelemetry);
            }
        }
    }
}