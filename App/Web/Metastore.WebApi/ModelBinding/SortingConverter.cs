﻿using System;
using System.ComponentModel;
using System.Globalization;
using Metastore.WebApi.Models;
using Metastore.WebApi.Models.Parameters;

namespace Metastore.WebApi.ModelBinding
{
    class SortingConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
            CultureInfo culture, object value)
        {
            if (value is string)
            {
                ListSorting sorting;
                if (ListSorting.TryParse((string)value, out sorting))
                {
                    return sorting;
                }
            }
            return base.ConvertFrom(context, culture, value);
        }
    }
}