﻿using System;
using System.ComponentModel;
using System.Globalization;
using Metastore.WebApi.Models.Parameters.Filters;

namespace Metastore.WebApi.ModelBinding
{
    public class FilterConverter<TFilter> : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var str = value as string;
            if (str != null)
            {
               
                if (typeof(TFilter) == typeof(TransactionsApiFilter))
                {
                    TransactionsApiFilter filter;

                    if (TransactionsApiFilter.TryParse(str, out filter))
                    {
                        return filter;
                    }
                }
                
                else
                {
                    throw new Exception("Unsupported filter type");
                }
            }

            return base.ConvertFrom(context, culture, value);
        }
    }
}