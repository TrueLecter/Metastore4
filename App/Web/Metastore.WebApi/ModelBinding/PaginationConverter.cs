﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using Metastore.WebApi.Models.Parameters;

namespace Metastore.WebApi.ModelBinding
{
    class PaginationConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
            CultureInfo culture, object value)
        {
            if (value is string)
            {
                ListPagination page;
                if (ListPagination.TryParse((string)value, out page))
                {
                    return page;
                }
            }
            return base.ConvertFrom(context, culture, value);
        }
    }
}