﻿using System.IO;
using System.Text;

namespace Metastore.WebApi.Filters
{
    public class CaptureStream : Stream
    {
        int _len;
        readonly Stream _responseStream;
        readonly StringBuilder _sb;

        public CaptureStream(Stream stream)
        {
            _sb = new StringBuilder();
            _responseStream = stream;
        }


        public string StreamAsString => _sb.ToString();
        public override bool CanRead => _responseStream.CanRead;

        public override bool CanSeek => _responseStream.CanSeek;

        public override bool CanWrite => _responseStream.CanWrite;

        public override long Length => _len;

        public override long Position
        {
            get { return _responseStream.Position; }

            set { _responseStream.Position = value; }
        }

        public override void Flush()
        {
            _responseStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _responseStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _responseStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _responseStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _len += count;

            _responseStream.Write(buffer, offset, count);
            _sb.Append(Encoding.UTF8.GetString(buffer, offset, count));
        }

        public override void Close()
        {
            _responseStream.Close();
        }
    }
}