﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FluentValidation;

namespace Metastore.WebApi.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Validate the model before control passes to the controller's action
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var contextRequest = actionContext.Request;

            if (!actionContext.ModelState.IsValid)
                throw new HttpResponseException(
                    contextRequest.CreateErrorResponse(HttpStatusCode.BadRequest,
                    actionContext.ModelState));

            foreach (var arg in actionContext.ActionArguments
                                    .Where(x => x.Value != null)
                                    .Select(x => x.Value))
            {
                var t = arg.GetType();

                if (!t.IsClass) continue;

                var validator = (IValidator) contextRequest.GetDependencyScope()
                                                    .GetService(typeof(IValidator<>).MakeGenericType(t));

                if (validator == null) continue;

                var result = validator.Validate(arg);

                if (!result.IsValid)
                    throw new HttpResponseException(
                        contextRequest.CreateErrorResponse(HttpStatusCode.BadRequest,
                            string.Join(",", result.Errors.Select(err => err.ErrorMessage))));
            }
        }
    }
}