﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace Metastore.WebApi.Filters
{
    public class CustomHeaderFilter: IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters==null)
                operation.parameters = new List<Parameter>();
            
            operation.parameters?.Add(new Parameter
            {
                description = "Public API calls must have referer header",
                name = "Referer",
                @in = "header",
                type = "string",
                required = false
            });

            operation.parameters?.Add(new Parameter
            {
                description = "Protected API calls must have authorization header with Bearer token",
                name = "Authorization",
                @in = "header",
                type = "string",
                required = false
            });
        }
    }
}