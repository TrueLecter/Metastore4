﻿using MediatR;

namespace Metastore.Infrastructure.Cqrs
{
    public interface IQuery<out TResult> : IRequest<TResult>
    {
    }
}
