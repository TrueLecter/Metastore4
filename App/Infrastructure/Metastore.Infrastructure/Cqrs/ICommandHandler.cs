﻿using MediatR;

namespace Metastore.Infrastructure.Cqrs
{
    // to remove mediatr, just remove the implementation of IRequest/IRequestHandler and add back in the Handle method
    public interface ICommandHandler<in TCommand, out TResult> : IRequestHandler<TCommand, TResult> where TCommand : ICommand<TResult> where TResult : struct
    {
        //TResult Handle(TCommand command);
    }

    // If void return values are desired from some (or all commands), you can switch to this interface. Note that MediatR always requires an output type, using "Unit" for void return types.
    //public interface ICommandHandler<TCommand> : IRequestHandler<TCommand, Unit> where TCommand : ICommand
    //{
    //    //void Handle(TCommand command);
    //}

    public interface IAsyncCommandHandler<in TCommand, TResult> : IAsyncRequestHandler<TCommand, TResult> where TCommand : ICommand<TResult> where TResult : struct
    { }
}
