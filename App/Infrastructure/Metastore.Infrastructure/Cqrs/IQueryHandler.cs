﻿using MediatR;

namespace Metastore.Infrastructure.Cqrs
{
    public interface IQueryHandler<in TQuery, out TResult> : IRequestHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
    }

    public interface IAsyncQueryHandler<in TQuery, TResult> : IAsyncRequestHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
    }
}
