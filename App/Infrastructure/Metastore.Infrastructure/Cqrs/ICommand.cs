﻿using MediatR;

namespace Metastore.Infrastructure.Cqrs
{
    public interface ICommand<out TResponse> : IRequest<TResponse> where TResponse : struct
    {
    }

    // note: you can also have void commands without return values by doing something like this: (Unit is from MediatR)
    //public interface ICommand : IRequest<Unit>
    //{
    //}
}
