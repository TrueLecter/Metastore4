﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;


namespace Metastore.Infrastructure.Azure
{
    // Dummy!
    public interface IAzureStorage
    {
        
    }

    public class AzureStorage : IAzureStorage
    {
        //readonly CloudBlobClient _blobClient;
        ////readonly StorageConfig _storageSettings;

        //public AzureStorage()
        //{
        //    //_storageSettings = externalSettingsProvider.GetSetting<StorageConfig>();
        //    //var storageAccount = _storageSettings.UseDevelopment
        //        //? CloudStorageAccount.DevelopmentStorageAccount
        //        //: new CloudStorageAccount(new StorageCredentials(_storageSettings.BlobStorageAccountName, _storageSettings.BlobStorageAccountKey), false);

        //    var storageAccount = new CloudStorageAccount(new StorageCredentials(), "", false); // dummy!

        //    _blobClient = storageAccount.CreateCloudBlobClient();
        //}

        //public async Task<byte[]> DownloadFileAsync(Uri uri)
        //{
        //    var containerName = GetContainerName(uri);
        //    if (string.IsNullOrWhiteSpace(containerName))
        //        throw new ArgumentException("Wrong container name provided");

        //    var fileName = GetFileName(uri);
        //    if (string.IsNullOrWhiteSpace(fileName))
        //        throw new ArgumentException("Wrong file name provided");

        //    var blobContainer = _blobClient.GetContainerReference(containerName);

        //    if (!await blobContainer.ExistsAsync())
        //        throw new ArgumentException("Blob container does not exist");

        //    var blob = blobContainer.GetBlockBlobReference(fileName);

        //    if (!await blob.ExistsAsync())
        //        throw new ArgumentException("Blob does not exist");
            
        //    var blobBytes = new byte[blob.Properties.Length];
        //    await blob.DownloadToByteArrayAsync(blobBytes, 0);
        //    return blobBytes;
        //}

        //public async Task<string> UploadFileAsync(string fileName, string containerName, string contentType, byte[] data)
        //{
        //    if (data == null)
        //        throw new ArgumentNullException(nameof(data));

        //    var blobContainer = _blobClient.GetContainerReference(containerName);
        //    if (!await blobContainer.ExistsAsync())
        //        throw new ArgumentException("Blob container does not exist", containerName);

        //    var blob = blobContainer.GetBlockBlobReference(fileName);
        //    if (await blob.ExistsAsync())
        //        throw new ArgumentException("File name must be unique", fileName);

        //    blob.Properties.ContentType = contentType;

        //    await blob.UploadFromByteArrayAsync(data, 0, data.Length);
        //    return blob.Uri.ToString();
        //}

        //public async Task DeleteFileAsync(Uri uri)
        //{
        //    var containerName = GetContainerName(uri);
        //    if (string.IsNullOrWhiteSpace(containerName))
        //        throw new ArgumentException("Wrong container name provided");

        //    var fileName = GetFileName(uri);
        //    if (string.IsNullOrWhiteSpace(fileName))
        //        throw new ArgumentException("Wrong file name provided");

        //    var blobContainer = _blobClient.GetContainerReference(containerName);

        //    if (!await blobContainer.ExistsAsync())
        //        throw new ArgumentException("Blob container does not exist");

        //    var blob = blobContainer.GetBlockBlobReference(fileName);

        //    if (!await blob.ExistsAsync())
        //        throw new ArgumentException("Blob does not exist");

        //    await blob.DeleteAsync();
        //}

        //#region Private methods

        //string GetFileName(Uri uri)
        //{
        //    return string.Join(string.Empty, uri.Segments.Skip(2).Select(Uri.UnescapeDataString));
        //}

        //string GetContainerName(Uri uri)
        //{
        //    return uri.Segments.Skip(1).Take(1).Select(x => x.Replace("/", "")).Select(Uri.UnescapeDataString).FirstOrDefault();
        //}

        //#endregion
    }
}
