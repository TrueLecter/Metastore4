﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Metastore.Infrastructure.UnitTesting.Db.Async;
using Moq;

namespace Metastore.Infrastructure.UnitTesting.Db.Context
{
    public static class ContextMock
    {
        // Mock a DbSet for syncronous queries (e.g. GetAll())
        public static Mock<DbSet<TEntity>> MockQuerySet<TEntity>(IQueryable<TEntity> data) where TEntity : class
        {
            var mockSet = new Mock<DbSet<TEntity>>();

            mockSet.As<IQueryable<TEntity>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<TEntity>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<TEntity>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<TEntity>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            return mockSet;
        }

        // Mock a DbSet for async queries (e.g. GetAsync())
        public static Mock<DbSet<TEntity>> MockAsyncQuerySet<TEntity>(IQueryable<TEntity> data) where TEntity : class
        {
            var mockSet = new Mock<DbSet<TEntity>>();

            mockSet.As<IDbAsyncEnumerable<TEntity>>()
                   .Setup(m => m.GetAsyncEnumerator())
                   .Returns(new TestDbAsyncEnumerator<TEntity>(data.GetEnumerator()));

            mockSet.As<IQueryable<TEntity>>()
                   .Setup(m => m.Provider)
                   .Returns(new TestDbAsyncQueryProvider<TEntity>(data.Provider));

            mockSet.As<IQueryable<TEntity>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<TEntity>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<TEntity>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            return mockSet;
        }

        public static Mock<TContext> MockContext<TContext>() where TContext : class
        {
            return new Mock<TContext>();
        }

        // Mock a DbSet for non-query methods (e.g. Create(), Update() etc)
        public static Mock<DbSet<TEntity>> MockNonQuerySet<TEntity>() where TEntity : class
        {
            return new Mock<DbSet<TEntity>>();
        }

        // Mock a DbContext with a given DbSet and an expression to get context data property, e.g. c => c.SomeEntities 
        public static void SetupDataMock<TContext, TEntity>(this Mock<TContext> contextMock, DataMock<TContext,TEntity> dataMock) where TContext : class
            where TEntity : class
        {
            contextMock.Setup(dataMock.DbSetPropertyExpression).Returns(dataMock.DbSetMock.Object);
        }

        public static void SetupDataMock<TContext, TEntity>(this Mock<TContext> contextMock, Mock<DbSet<TEntity>> dataMock)
            where TContext : DbContext
            where TEntity : class
        {
            contextMock.Setup(mc => mc.Set<TEntity>()).Returns(dataMock.Object);
        }
    }

    public class DataMock<TContext, TEntity> where TEntity : class
    {
        public Mock<DbSet<TEntity>> DbSetMock { get; set; }
        public Expression<Func<TContext, DbSet<TEntity>>> DbSetPropertyExpression { get; set; }
    }
}
