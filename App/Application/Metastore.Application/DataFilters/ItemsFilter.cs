﻿using System;
using System.Linq;
using Metastore.Application.Contracts.Models;
using Metastore.Data.Contracts;
using Metastore.Data.Stores;
using Metastore.Domain.StandardTypes;

namespace Metastore.Application.DataFilters
{
    public class ItemsFilter : IQueryFilter<Item>
    {
        public ItemType[] Types { get; set; }
        public IQueryable<Item> Apply(IQueryable<Item> query)
        {
            throw new NotImplementedException();
        }
    }
}