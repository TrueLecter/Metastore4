﻿using Metastore.Domain.StandardTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metastore.Data.Contracts;
using Metastore.Data.Stores;

namespace Metastore.Application.DataFilters
{
    public class TransactionsFilter : IQueryFilter<Transaction>
    {
        public IList<TransactionType> Types { get; set; }

        public IQueryable<Transaction> Apply(IQueryable<Transaction> query)
            => query.Where(t => Types.Contains((TransactionType)t.Type));
    }
}
