﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metastore.Application
{
    public abstract class AbstractHandler
    {
        protected readonly IMapper _mapper;
        
        public AbstractHandler(IMapper mapper)
        {
            _mapper = mapper;
        }


        public class CommonMappers : Profile
        {
            public CommonMappers()
            {
                // Define common mapping profiles here
                // CreateMap<>()...
            }
        }
    }
}
