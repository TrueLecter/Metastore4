﻿using System;
using AutoMapper;
using Metastore.Application.Contracts.Commands.Protected.WishlistCommands;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.WishlistCommandHandlers
{
    public class AddItemToWishlistCommandHandler :
        AbstractHandler,
        ICommandHandler<AddItemToWishlistCommand, CreateResult<bool>>
    {

        private readonly StoresDataContext _wishlist;

        public AddItemToWishlistCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _wishlist = transactions;
        }

        public CreateResult<bool> Handle(AddItemToWishlistCommand message)
        {
            var query = _wishlist.Wishlists;
            try
            {
                var entity = new Wishlist { UserId = message.UserId, ItemId = message.ItemId, DateAdded = DateTime.Now};
                query.Add(entity);
                _wishlist.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return CreateResult<bool>.Nok(e.Message);
            }
            return CreateResult<bool>.Ok(true);

        }

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<WishlistItemModel, Wishlist>();
            }
        }
    }

   
}