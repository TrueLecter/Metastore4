﻿using System;
using System.Linq;
using AutoMapper;
using EntityFramework.Extensions;
using Metastore.Application.Contracts.Commands.Protected.WishlistCommands;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.WishlistCommandHandlers
{
    public class DeleteItemFromWishlistCommandHandler :
        AbstractHandler,
        ICommandHandler<DeleteItemFromWishlistCommand, DeleteResult>
    {

        private readonly StoresDataContext _wishlist;

        public DeleteItemFromWishlistCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _wishlist = transactions;
        }

        public DeleteResult Handle(DeleteItemFromWishlistCommand message)
        {
            var query = _wishlist.Wishlists;
            try
            {
                query.Where(item => item.ItemId == message.ItemId && item.UserId == message.UserId).Delete();
                _wishlist.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return DeleteResult.Nok(e.Message);
            }
            return DeleteResult.Ok();

        }
    }
}