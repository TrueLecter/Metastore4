﻿using System;
using AutoMapper;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.StoreCommands;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.StoreCommandHandlers
{
    public class CreateStoreCommandHandler :
        AbstractHandler,
        ICommandHandler<CreateStoreCommand, CreateResult<int>>
    {

        private readonly StoresDataContext _stores;

        public CreateStoreCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _stores = transactions;
        }

        public CreateResult<int> Handle(CreateStoreCommand message)
        {
            var query = _stores.Stores;
            int id = 0;
            try
            {
                var entity = _mapper.Map<StoreModel, Store>(message.NewStore);
                query.Add(entity);
                _stores.SaveChanges();
                id = entity.Id;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return CreateResult<int>.Nok(e.Message);
            }
            return CreateResult<int>.Ok(id);

        }

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<StoreModel, Store>();
            }
        }
    }

   
}