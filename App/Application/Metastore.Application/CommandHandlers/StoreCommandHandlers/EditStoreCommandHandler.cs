﻿using System;
using System.Data.Entity.Migrations;
using AutoMapper;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.StoreCommands;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.StoreCommandHandlers
{
    public class EditStoreCommandHandler :
        AbstractHandler,
        ICommandHandler<EditStoreCommand, UpdateResult>
    {

        private readonly StoresDataContext _Stores;

        public EditStoreCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _Stores = transactions;
        }

        public UpdateResult Handle(EditStoreCommand message)
        {
            var query = _Stores.Stores;
            try
            {
                var entity = _mapper.Map<StoreModel, Store>(message.UpdateStore);
                query.AddOrUpdate(entity);
                _Stores.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return UpdateResult.Nok(e.Message);
            }
            return UpdateResult.Ok();

        }

       
    }


}