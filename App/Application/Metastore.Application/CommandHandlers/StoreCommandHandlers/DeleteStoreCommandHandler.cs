﻿using System;
using System.Linq;
using AutoMapper;
using EntityFramework.Extensions;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.StoreCommands;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.StoreCommandHandlers
{
    public class DeleteStoreCommandHandler :
        AbstractHandler,
        ICommandHandler<DeleteStoreCommand, DeleteResult>
    {

        private readonly StoresDataContext _stores;

        public DeleteStoreCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _stores = transactions;
        }

        public DeleteResult Handle(DeleteStoreCommand message)
        {
            var query = _stores.Stores;
            try
            {
                query.Where(s => s.Id == message.Id).Delete();
                _stores.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return DeleteResult.Nok(e.Message);
            }
            return DeleteResult.Ok();

        }
    }
}