﻿using System;
using System.Linq;
using AutoMapper;
using EntityFramework.Extensions;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.ItemCommands;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.ItemCommandHandlers
{
    public class DeleteItemCommandHandler :
        AbstractHandler,
        ICommandHandler<DeleteItemCommand, DeleteResult>
    {

        private readonly StoresDataContext _items;

        public DeleteItemCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _items = transactions;
        }

        public DeleteResult Handle(DeleteItemCommand message)
        {
            var query = _items.Items;
            try
            {
                query.Where(item => item.Id == message.Id).Delete();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return DeleteResult.Nok(e.Message);
            }
            return DeleteResult.Ok();

        }
    }
}