﻿using System;
using System.Data.Entity.Migrations;
using AutoMapper;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.ItemCommands;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.ItemCommandHandlers
{
    public class EditItemCommandHandler :
        AbstractHandler,
        ICommandHandler<EditItemCommand, UpdateResult>
    {

        private readonly StoresDataContext _items;

        public EditItemCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _items = transactions;
        }

        public UpdateResult Handle(EditItemCommand message)
        {
            var query = _items.Items;
            try
            {
                var entity = _mapper.Map<ItemModel, Item>(message.UpdateItem);
                query.AddOrUpdate(entity);
                _items.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return UpdateResult.Nok(e.Message);
            }
            return UpdateResult.Ok();

        }

       
    }


}