﻿using System;
using AutoMapper;
using Metastore.Application.Contracts.Commands.Protected;
using Metastore.Application.Contracts.Commands.Protected.ItemCommands;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.CommandHandlers.ItemCommandHandlers
{
    public class CreateItemCommandHandler :
        AbstractHandler,
        ICommandHandler<CreateItemCommand, CreateResult<int>>
    {

        private readonly StoresDataContext _items;

        public CreateItemCommandHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _items = transactions;
        }

        public CreateResult<int> Handle(CreateItemCommand message)
        {
            var query = _items.Items;
            int id;
            try
            {
                var entity = _mapper.Map<ItemModel, Item>(message.NewItem);
                query.Add(entity);
                _items.SaveChanges();
                id = entity.Id;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return CreateResult<int>.Nok(e.Message);
            }
            return CreateResult<int>.Ok(id);

        }

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<ItemModel, Item>()
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => s.Type))
                    .ForMember(d => d.Currency, opt => opt.MapFrom(s => s.Currency));
            }
        }
    }

   
}