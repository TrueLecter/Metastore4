﻿using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EntityFramework.Extensions;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Data.Extensions;
using Metastore.Data.Stores;
using Metastore.Domain.StandardTypes;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.QueryHandlers.StoreQueryHandlers
{
    public class StoresQueriesHandler:
        AbstractHandler,
        IAsyncQueryHandler<StoresListQuery, ListPage<StoreModel>>
    {
        private readonly StoresDataContext _stores;
        public StoresQueriesHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _stores = transactions;
        }

        public async Task<ListPage<StoreModel>> Handle(StoresListQuery message)
        {
            var basequery = _stores.Stores;

            var allTransactionsCount = basequery.FutureCount();

            var resultStores = await basequery
                .WithFilter(message.Filter)
                .WithParams(message.SelectParams)
                .ProjectTo<StoreModel>(_mapper.ConfigurationProvider).Future().ToListAsync();

            return new ListPage<StoreModel>(resultStores, allTransactionsCount);
        }
        
        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<Store, StoreModel>()
                    .ForMember(d => d.Items, opt => opt.MapFrom(s => s.Items))
                    .ForMember(d => d.Currency, opt => opt.MapFrom(s => (Currency)s.Currency));
            }
        }
    }
}