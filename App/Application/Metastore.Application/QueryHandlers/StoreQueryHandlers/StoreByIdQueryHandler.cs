﻿using System;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.QueryHandlers.StoreQueryHandlers
{
    public class StoreByIdQueriesHandler :
        AbstractHandler,
        IQueryHandler<StoreByIdQuery, StoreModel>
    {
        private readonly StoresDataContext _items;

        public StoreByIdQueriesHandler(IMapper mapper, StoresDataContext context) : base(mapper)
        {
            _items = context;
        }

        public StoreModel Handle(StoreByIdQuery message)
        {
            var basequery = _items.Stores;

            //var allTransactionsCount = basequery.FutureCount();

            var resultItem = basequery
                .Where(item => item.Id == message.Id)
                .ProjectTo<StoreModel>(_mapper.ConfigurationProvider)
                .Single();
            if (resultItem == null) throw new ArgumentNullException(nameof(resultItem));

            return resultItem;
        }
    }
}