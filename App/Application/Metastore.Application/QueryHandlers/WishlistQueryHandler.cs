﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Cqrs.QueryHandlers
{
    public class WishlistQueriesHandler :
        AbstractHandler,
        IQueryHandler<WishlistQuery, List<WishlistItemModel>>
    {
        private readonly StoresDataContext _items;

        public WishlistQueriesHandler(IMapper mapper, StoresDataContext context) : base(mapper)
        {
            _items = context;
        }

        public List<WishlistItemModel> Handle(WishlistQuery message)
        {
            var basequery = _items.Wishlists;

            var resultItem = basequery
                .Where(item => item.UserId == message.UserId)
                .ProjectTo<WishlistItemModel>(_mapper.ConfigurationProvider).ToList();

            return resultItem;
        }

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<Wishlist, WishlistItemModel>()
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Item.Name))
                    .ForMember(d => d.Seller, opt => opt.MapFrom(s => s.Item.SellerName))
                    .ForMember(d => d.Price, opt => opt.MapFrom(s => s.Item.Price))
                    .ForMember(d => d.Aviable, opt => opt.MapFrom(s => s.Item.Quantity > 0 && s.Item.Enabled))
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item.Id));
            }
        }
    }
}