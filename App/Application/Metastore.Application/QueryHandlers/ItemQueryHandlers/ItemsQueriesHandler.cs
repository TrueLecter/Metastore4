﻿using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EntityFramework.Extensions;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Data.Extensions;
using Metastore.Data.Stores;
using Metastore.Domain.StandardTypes;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.QueryHandlers.ItemQueryHandlers
{
    public class ItemsQueriesHandler:
        AbstractHandler,
        IAsyncQueryHandler<ItemsListQuery, ListPage<ItemModel>>
    {
        private readonly StoresDataContext _items;
        public ItemsQueriesHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _items = transactions;
        }

        public async Task<ListPage<ItemModel>> Handle(ItemsListQuery message)
        {
            var basequery = _items.Items;

            var allTransactionsCount = basequery.FutureCount();

            var resultTransactions = await basequery
                .WithFilter(message.Filter)
                .WithParams(message.SelectParams)
                .ProjectTo<ItemModel>(_mapper.ConfigurationProvider).Future().ToListAsync();

            return new ListPage<ItemModel>(resultTransactions, allTransactionsCount);
        }
        
        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<Item, ItemModel>()
                    .ForMember(d => d.Price, opt => opt.MapFrom(s => s.Price))
                    .ForMember(d => d.Width, opt => opt.MapFrom(s => s.Width ?? 0.0))
                    .ForMember(d => d.Height, opt => opt.MapFrom(s => s.Height ?? 0.0))
                    .ForMember(d => d.Weight, opt => opt.MapFrom(s => s.Weight ?? 0.0))
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => (ItemType)s.Type))
                    .ForMember(d => d.Currency, opt => opt.MapFrom(s => (Currency)s.Type));
            }
        }
    }
}