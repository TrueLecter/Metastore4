﻿using System;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.QueryHandlers.ItemQueryHandlers
{
    public class ItemByIdQueriesHandler :
        AbstractHandler,
        IQueryHandler<ItemByIdQuery, ItemModel>
    {
        private readonly StoresDataContext _items;

        public ItemByIdQueriesHandler(IMapper mapper, StoresDataContext context) : base(mapper)
        {
            _items = context;
        }

        public ItemModel Handle(ItemByIdQuery message)
        {
            var basequery = _items.Items;

            //var allTransactionsCount = basequery.FutureCount();

            var resultItem = basequery
                .Where(item => item.Id == message.Id)
                .ProjectTo<ItemModel>(_mapper.ConfigurationProvider)
                .Single();
            if (resultItem == null) throw new ArgumentNullException(nameof(resultItem));

            return resultItem;
        }
    }


   
}