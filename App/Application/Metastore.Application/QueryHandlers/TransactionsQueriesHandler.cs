using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EntityFramework.Extensions;
using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Application.Contracts.Queries.Protected;
using Metastore.Data.Extensions;
using Metastore.Data.Stores;
using Metastore.Domain.StandardTypes;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.QueryHandlers
{
    public class TransactionsQueriesHandler : 
        AbstractHandler,
        IAsyncQueryHandler<TransactionsListQuery, ListPage<TransactionModel>>
    {
        private readonly StoresDataContext _transactions;
        public TransactionsQueriesHandler(IMapper mapper, StoresDataContext transactions) : base(mapper)
        {
            _transactions = transactions;
        }


        public async Task<ListPage<TransactionModel>> Handle(TransactionsListQuery message)
        {
            var basequery = _transactions.Transactions;

            var allTransactionsCount = basequery.FutureCount();
            
            var resultTransactions = await basequery
                .WithFilter(message.Filter) // Filtering must be applied before sorting/paging!
                .WithParams(message.SelectParams)
                .ProjectTo<TransactionModel>(_mapper.ConfigurationProvider).Future().ToListAsync();

            return new ListPage<TransactionModel>(resultTransactions, allTransactionsCount);
        }



        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<Transaction, TransactionModel>()
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => (TransactionType) s.Type))
                    .ForMember(d => d.FriendlyId, opt => opt.MapFrom(s => s.ReferenceId));
            }
        }
    }
}