﻿using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Data.Contracts;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Queries.Protected
{
   public class ItemsListQuery : IQuery<ListPage<ItemModel>>
    {
        public ItemsListQuery(IQueryFilter<Item> filter, SelectParams selectParams)
        {
            Filter = filter;
            SelectParams = selectParams;
        }
        

        public IQueryFilter<Item> Filter { get; }
        public SelectParams SelectParams { get; }
    }
}