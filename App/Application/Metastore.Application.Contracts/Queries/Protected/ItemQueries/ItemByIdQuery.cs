﻿using Metastore.Application.Contracts.Models;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Queries.Protected
{
    public class ItemByIdQuery : IQuery<ItemModel>
    {
        public ItemByIdQuery(int id)
        {
         //   Filter = filter;
            Id = id;
        }


        //public IQueryFilter<Item> Filter { get; }
        public int Id { get; }
    }
}