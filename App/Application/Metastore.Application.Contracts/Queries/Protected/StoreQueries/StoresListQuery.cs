﻿using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Data.Contracts;
using Metastore.Data.Stores;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Queries.Protected
{
   public class StoresListQuery : IQuery<ListPage<StoreModel>>
    {
        public StoresListQuery(IQueryFilter<Store> filter, SelectParams selectParams)
        {
            Filter = filter;
            SelectParams = selectParams;
        }
        

        public IQueryFilter<Store> Filter { get; }
        public SelectParams SelectParams { get; }
    }
}