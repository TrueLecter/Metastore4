﻿using Metastore.Application.Contracts.Models;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Queries.Protected
{
    public class StoreByIdQuery : IQuery<StoreModel>
    {
        public StoreByIdQuery(int id)
        {
            Id = id;
        }
        public int Id { get; }
    }
}