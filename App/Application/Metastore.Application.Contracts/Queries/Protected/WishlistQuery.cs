using System.Collections.Generic;
using Metastore.Application.Contracts.Models;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Queries.Protected
{
    public class WishlistQuery : IQuery<List<WishlistItemModel>>
    {
        public WishlistQuery(string userId)
        {
            UserId = userId;
        }
        
        public string UserId { get; }
    }
    
}