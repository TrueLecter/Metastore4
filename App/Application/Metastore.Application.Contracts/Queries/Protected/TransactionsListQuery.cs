using System.Collections.Generic;
using Metastore.Application.Contracts.Models;
using Metastore.Infrastructure.Cqrs;
using Metastore.Application.Contracts.Models.Common;
using Metastore.Data.Contracts;
using Metastore.Data.Stores;

namespace Metastore.Application.Contracts.Queries.Protected
{
    public class TransactionsListQuery : IQuery<ListPage<TransactionModel>>
    {
        public TransactionsListQuery(IQueryFilter<Transaction> filter, SelectParams selectParams)
        {
            Filter = filter;
            SelectParams = selectParams;
        }

        public IQueryFilter<Transaction> Filter { get; }
        public SelectParams SelectParams { get; }
    }

    // Dummy!
}