﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metastore.Application.Contracts.Results
{
    public struct CreateResult<T>
    {
        public T CreatedEntityKey { get; }
        public bool Success { get; }

        public string Error { get; }

        public string Logs { get; set; }

        private CreateResult(T key)
        {
            CreatedEntityKey = key;
            Success = true;
            Error = null;
            Logs = null;
        }

        private CreateResult(string error, string logs = null)
        {
            Success = false;
            Error = error;
            CreatedEntityKey = default(T);
            Logs = logs;
        }

        public static CreateResult<T> Ok(T key) => new CreateResult<T>(key);
        public static CreateResult<T> Nok(string error, string logs = null) => new CreateResult<T>(error, logs);

    }

}
