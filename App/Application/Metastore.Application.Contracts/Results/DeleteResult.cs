﻿namespace Metastore.Application.Contracts.Results
{
    public struct DeleteResult
    {
        public bool Success { get; }

        public string Error { get; }

        public string Logs { get; set; }

        private DeleteResult(bool success, string error = null, string logs = null)
        {
            Success = success;
            Error = error;
            Logs = logs;
        }

        public static DeleteResult Ok() => new DeleteResult(true);
        public static DeleteResult Nok(string error, string logs = null) => new DeleteResult(false, error, logs);

    }

}
