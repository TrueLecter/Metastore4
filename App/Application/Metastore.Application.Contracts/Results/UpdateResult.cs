﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metastore.Application.Contracts.Results
{
    public struct UpdateResult
    {
        public bool Success { get; }

        public string Error { get; }

        public string Logs { get; set; }

        private UpdateResult(bool success, string error = null, string logs = null)
        {
            Success = success;
            Error = error;
            Logs = logs;
        }

        public static UpdateResult Ok() => new UpdateResult(true);
        public static UpdateResult Nok(string error, string logs = null) => new UpdateResult(false, error, logs);

    }

}
