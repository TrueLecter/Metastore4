﻿using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Commands.Protected.StoreCommands
{
    public class EditStoreCommand : ICommand<UpdateResult>  
    {
        public EditStoreCommand(StoreModel m)
        {
            UpdateStore = m;
        }
        public StoreModel UpdateStore { get; }
    }
}