﻿using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Commands.Protected.StoreCommands
{
    public class CreateStoreCommand : ICommand<CreateResult<int>>  
    {
        public CreateStoreCommand(StoreModel m)
        {
            NewStore = m;
        }
        public StoreModel NewStore { get; }
    }
}