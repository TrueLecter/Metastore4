﻿using Metastore.Application.Contracts.Results;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Commands.Protected.StoreCommands
{
    public class DeleteStoreCommand : ICommand<DeleteResult>  
    {
        public DeleteStoreCommand(int id)
        {
            Id = id;
        }
        public int Id { get; }
    }
}