﻿using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Commands.Protected.ItemCommands
{
    public class CreateItemCommand : ICommand<CreateResult<int>>  
    {
        public CreateItemCommand(ItemModel m)
        {
            NewItem = m;
        }
        public ItemModel NewItem { get; }
    }
}