﻿using Metastore.Application.Contracts.Results;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Commands.Protected.ItemCommands
{
    public class DeleteItemCommand : ICommand<DeleteResult>  
    {
        public DeleteItemCommand(int id)
        {
            Id = id;
        }
        public int Id { get; }
    }
}