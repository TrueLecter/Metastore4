﻿using Metastore.Application.Contracts.Models;
using Metastore.Application.Contracts.Results;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Commands.Protected.ItemCommands
{
    public class EditItemCommand : ICommand<UpdateResult>  
    {
        public EditItemCommand(ItemModel m)
        {
            UpdateItem = m;
        }
        public ItemModel UpdateItem { get; }
    }
}