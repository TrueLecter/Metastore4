﻿using Metastore.Application.Contracts.Results;
using Metastore.Infrastructure.Cqrs;

namespace Metastore.Application.Contracts.Commands.Protected.WishlistCommands
{
    public class AddItemToWishlistCommand : ICommand<CreateResult<bool>>  
    {
        public AddItemToWishlistCommand(string userId, int itemId)
        {
            UserId = userId;
            ItemId = itemId;
        }
        public string UserId { get; }
        public int ItemId { get; }
    }
}