﻿using Metastore.Domain.StandardTypes;
using System;
using System.Reflection.Emit;

namespace Metastore.Application.Contracts.Models
{
    public class TransactionModel
    {
        public int Id { get; set; }

        public TransactionType Type { get; set; }


        public string FriendlyId { get; set; }

        public decimal Amount { get; set; }



    }

}