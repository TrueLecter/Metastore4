﻿using System.Collections.Generic;
using Metastore.Domain.StandardTypes;

namespace Metastore.Application.Contracts.Models
{
    public class StoreModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public string Owner { get; set; }

        public Currency Currency { get; set; }

        public List<ItemModel> Items { get; set; }
    }
}