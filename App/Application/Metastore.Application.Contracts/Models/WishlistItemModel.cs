﻿using System;
using System.Collections.Generic;

namespace Metastore.Application.Contracts.Models
{
    public class WishlistItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Seller { get; set; }
        public double Price { get; set; }
        public bool Aviable { get; set; }
        public DateTime DateAdded { get; set; }
    }
}