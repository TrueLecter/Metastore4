﻿using Metastore.Domain.StandardTypes;
using System;
using System.IO;
using System.Net.Mime;

namespace Metastore.Application.Contracts.Models
{

    public class ItemModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public string SellerName { get; set; }

        public double Price { get; set; }

        public Currency Currency { get; set; }

        public int Quantity { get; set; }
        
        //Images URI
        public string Images { get; set; }

        public string Thumbnail { get; set; }

        public bool Enabled { get; set; } 

        public ItemType Type { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public double Weight { get; set; }

        //File URI
        public string DigitalFile { get; set; }

        public string Tags { get; set; }

        public int StoreId { get; set; }
    }
}