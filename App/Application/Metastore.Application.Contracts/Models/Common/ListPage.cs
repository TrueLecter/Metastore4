﻿using System.Collections;
using System.Collections.Generic;

namespace Metastore.Application.Contracts.Models.Common
{
    public class ListPage<T>: ICollection<T>
    {
        private readonly ICollection<T> _pageItems;
        public int TotalCount { get; }

        public ListPage() { }

        public ListPage(ICollection<T> pageItems, int totalCount)
        {
            _pageItems = pageItems;
            TotalCount = totalCount;
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            return _pageItems.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            throw new System.NotImplementedException();
        }

        public void Clear()
        {
            throw new System.NotImplementedException();
        }

        public bool Contains(T item)
        {
            return _pageItems.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _pageItems.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            throw new System.NotImplementedException();
        }

        public int Count => _pageItems.Count;
        public bool IsReadOnly => _pageItems.IsReadOnly;
    }
}