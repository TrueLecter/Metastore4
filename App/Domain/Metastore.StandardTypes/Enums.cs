﻿
namespace Metastore.Domain.StandardTypes
{
    public enum TransactionType
    {
        Debit = 1,
        Credit = 2,
    }

    public enum TransactionStatus
    {
        Pending = 1,
        Success = 2,
        Failed = 3
    }

    public enum PaymentMethod
    {
        Check = 1,
        Wire = 2,
        Ach = 3
    }

    public enum Currency
    {
        USD = 1,
        UAH = 2,
        EUR = 3,
    }

    public enum ItemType
    {
        Digital = 1,
        Material = 2,
    }

}
