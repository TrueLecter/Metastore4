﻿using Metastore.Data.Contracts;
using System.Linq;

namespace Metastore.Data.Extensions
{
    public static class QueryableFilterExtensions
    {
        public static IQueryable<T> WithFilter<T>(this IQueryable<T> query, IQueryFilter<T> filter) => filter!=null ? filter.Apply(query) : query;
    }
}