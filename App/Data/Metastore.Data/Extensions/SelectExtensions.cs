﻿using Metastore.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Metastore.Data.Extensions
{
    public static class TypeEx
    {
        public static void GetNestedProperty(this Type t, string propName, IList<PropertyInfo> infos)
        {
            var properties = t.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (properties.Count(p => p.Name == propName.Split('.')[0]) == 0)
                throw new ArgumentNullException($"Property {propName}, is not exists in object {t}");

            if (propName.Split('.').Length == 1)
            {
                infos.Add(t.GetProperty(propName));
                return;
            }

            if (infos.Count == 0)
            {
                infos.Add(t.GetProperty(propName.Split('.')[0]));
            }

            GetNestedProperty(t.GetProperty(propName.Split('.')[0]).PropertyType, propName.Split('.')[1], infos);
        }

        public static MemberExpression BuildExpression(this ParameterExpression param, IList<PropertyInfo> infos)
        {
            var me = Expression.Property(param, infos[0]);
            for (int i = 1; i < infos.Count - 1; i++)
            {
                me = Expression.Property(me, infos[i]);
            }
            return me;
        }
    }

    public static class SelectExtensions
    {
        public static IQueryable<T> WithParams<T>(this IQueryable<T> query, SelectParams selectParams) where T : class
        {
            if (selectParams == null)
                return query;

            if (!string.IsNullOrEmpty(selectParams.SortField))
            {
                query = query.OrderUsingSortExpression(selectParams.SortField,selectParams.SortDesc);
            }

            if (selectParams.Take.HasValue)
            {
                query = query.Skip(selectParams.Skip).Take(selectParams.Take.Value);
            }

            return query;
        }

        public static IOrderedQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string orderByProperty, bool desc) where TEntity : class
        {
            var command = desc ? "OrderByDescending" : "OrderBy";
            var type = typeof(TEntity);
            var infos = new List<PropertyInfo>();
            type.GetNestedProperty(orderByProperty, infos);
            var property = infos[infos.Count - 1];
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = infos.Count == 1 ?
                Expression.MakeMemberAccess(parameter, property) :
                Expression.MakeMemberAccess(parameter.BuildExpression(infos), property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new[] { type, property.PropertyType }, source.Expression, Expression.Quote(orderByExpression));
            return (IOrderedQueryable<TEntity>)source.Provider.CreateQuery<TEntity>(resultExpression);
        }


        public static IQueryable<TEntity> Filter<TEntity>(this IQueryable<TEntity> sequence, SearchQuery<TEntity> query)
        {
            if (query.Filters != null && query.Filters.Count > 0)
            {
                query.Filters.ForEach(filter => sequence = sequence.Where(filter));
            }

            return sequence;
        }

        private static LambdaExpression GenerateSelector<TEntity>(string propertyName, out Type resultType) where TEntity : class
        {
            var parameter = Expression.Parameter(typeof(TEntity), "Entity");

            PropertyInfo property;
            Expression propertyAccess;
            if (propertyName.Contains('.'))
            {
                var childProperties = propertyName.Split('.');
                property = typeof(TEntity).GetProperty(childProperties[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
                for (var i = 1; i < childProperties.Length; i++)
                {
                    property = property.PropertyType.GetProperty(childProperties[i], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    propertyAccess = Expression.MakeMemberAccess(propertyAccess, property);
                }
            }
            else
            {
                property = typeof(TEntity).GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
            }
            resultType = property.PropertyType;

            return Expression.Lambda(propertyAccess, parameter);
        }

        private static MethodCallExpression GenerateMethodCall<TEntity>(IQueryable<TEntity> source, string methodName, String fieldName) where TEntity : class
        {
            var type = typeof(TEntity);
            Type selectorResultType;
            var selector = GenerateSelector<TEntity>(fieldName, out selectorResultType);
            var resultExp = Expression.Call(typeof(Queryable), methodName,
                new Type[] { type, selectorResultType },
                source.Expression, Expression.Quote(selector));
            return resultExp;
        }

        public static IOrderedQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            var resultExp = GenerateMethodCall<TEntity>(source, "OrderBy", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }

        public static IOrderedQueryable<TEntity> OrderByDescending<TEntity>(this IQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            var resultExp = GenerateMethodCall<TEntity>(source, "OrderByDescending", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }

        public static IOrderedQueryable<TEntity> ThenBy<TEntity>(this IOrderedQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            var resultExp = GenerateMethodCall<TEntity>(source, "ThenBy", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }

        public static IOrderedQueryable<TEntity> ThenByDescending<TEntity>(this IOrderedQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            var resultExp = GenerateMethodCall<TEntity>(source, "ThenByDescending", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }

        public static IOrderedQueryable<TEntity> OrderUsingSortExpression<TEntity>(this IQueryable<TEntity> source, string sortExpression, bool isDesc) where TEntity : class
        {
            var sortProperty = GetSortPropertyInfoInEntity<TEntity>(sortExpression) != null ? sortExpression : GetFallbackSortPropertyFromEntity<TEntity>();
            return isDesc ? source.OrderByDescending(sortProperty) : source.OrderBy(sortProperty);
        }

        static string GetFallbackSortPropertyFromEntity<TEntity>()
        {
            var simpleProperty = typeof(TEntity).GetProperties().FirstOrDefault(x => IsSimple(x.PropertyType));

            if (simpleProperty != null)
                return simpleProperty.Name;

            var innerProperties = from parentType in typeof(TEntity).GetProperties()
                                  from childProperty in parentType.PropertyType.GetProperties()
                                  where IsSimple(childProperty.PropertyType)
                                  select $"{parentType.Name}.{childProperty.Name}";

            return innerProperties.First();
        }

        static PropertyInfo GetSortPropertyInfoInEntity<TEntity>(string propertyName)
        {
            PropertyInfo propertyInfo = null;
            var entityType = typeof(TEntity);

            foreach (var propertyNamePart in propertyName.Trim().Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries))
            {
                propertyInfo = propertyInfo == null ? entityType.GetProperty(propertyNamePart) : propertyInfo.PropertyType.GetProperty(propertyNamePart);

                if (propertyInfo == null)
                    return null;
            }

            return propertyInfo;
        }

        static bool IsSimple(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // nullable type, check if the nested type is simple.
                return IsSimple(type.GetGenericArguments()[0]);
            }
            return type.IsPrimitive
                   || type.IsEnum
                   || type == typeof(string)
                   || type == typeof(decimal)
                   || type == typeof(DateTime);
        }

    }
}
