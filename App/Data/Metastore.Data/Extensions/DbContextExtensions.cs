using System.Data.Common;
using System.Data.Entity;

namespace Metastore.Data.Extensions
{
    public static class DbContextExtensions
    {
        public static DbContext SetSessionContextParam<T>(this DbContext context, DbConnection connection, string paramName, T paramValue)
        {
            if (paramValue == null || string.IsNullOrEmpty(paramName))
                return context;

            // Set SESSION_CONTEXT to current UserId whenever EF opens a connection
            try
            {
                DbCommand cmd = connection.CreateCommand();
                cmd.CommandText = $"EXEC sp_set_session_context @key=N'{paramName}', @value=@paramValue";
                DbParameter param = cmd.CreateParameter();
                param.ParameterName = "@paramValue";

                param.Value = paramValue;


                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
            }
            catch (System.NullReferenceException)
            {
                // leave SESSION_CONTEXT null (all rows will be filtered)
            }

            return context;
        }

        public static DbContext SetUserContext(this DbContext context, DbConnection connection, string userId)
        {
            return context.SetSessionContextParam(connection, "user_id", userId);
        }

        public static DbContext SetStoreTenantContext(this DbContext context, DbConnection connection, int storeId)
        {
            return context.SetSessionContextParam(connection, "store_id", storeId);
        }

        public static DbContext AllowSharedAccess(this DbContext context, DbConnection connection)
        {
            DbCommand cmd = connection.CreateCommand();
            cmd.CommandText = "EXEC sp_set_session_context @key=N'allow_shared_access', @value=1";
            cmd.ExecuteNonQuery();
            return context;
        }
    }
}
