﻿using Metastore.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metastore.Data.Contexts
{
    public class AuthDbContext: IdentityDbContext<ApplicationUser>
    {
        public static AuthDbContext Create()
        {
            return new AuthDbContext();
        }
    }
}
