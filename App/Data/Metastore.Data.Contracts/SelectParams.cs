﻿namespace Metastore.Data.Contracts
{
    public class SelectParams
    {
        public int Skip { get; set; }
        public int? Take { get; set; } // null to take all

        public string SortField { get; set; }

        public bool SortDesc { get; set; }
    }
}
