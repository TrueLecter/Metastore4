﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Metastore.Data.Contracts
{
    public class SearchQuery<TEntity>
    {
        public SearchQuery()
        {
            Filters = new List<Expression<Func<TEntity, bool>>>();
        }

        public List<Expression<Func<TEntity, bool>>> Filters { get; protected set; }

        public void AddFilter(Expression<Func<TEntity, bool>> filter)
        {
            Filters.Add(filter);
        }
    }
}
