﻿using System.Threading.Tasks;

namespace Metastore.Data.Contracts
{
    public interface IAsyncRepository<TEntity, in TKey>
    {
        Task<TEntity> GetAsync(TKey key);
        Task<TEntity> CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TKey key);
    }
}