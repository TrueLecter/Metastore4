﻿using System.Linq;

namespace Metastore.Data.Contracts
{
    public interface IQueryFilter<T>
    {
        IQueryable<T> Apply(IQueryable<T> query);
    }
}