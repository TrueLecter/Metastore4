﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metastore.Data.Contracts
{
    public interface IRepository<TEntity, in TKey>
    {
        
        TEntity Get(TKey key);
        TEntity Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TKey key);
    }
}
