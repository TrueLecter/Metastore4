using System.Linq;

namespace Metastore.Data.Contracts
{
    public interface IDataView<out T>
    {
        IQueryable<T> Build();
    }
}