﻿CREATE TABLE [pmnt].[Transactions] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Type] INT NOT NULL DEFAULT (0), 
    [ReferenceId] NVARCHAR (100)  NOT NULL,
    [Amount]      DECIMAL (18, 6) NOT NULL,
    [CreatedOn]   DATETIME        NOT NULL,
    [CreatedBy]   NVARCHAR (50)   NOT NULL,
    CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED ([Id] ASC)
);

