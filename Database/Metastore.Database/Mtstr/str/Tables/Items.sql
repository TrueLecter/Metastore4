﻿CREATE TABLE [str].[Items] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Price] float  NOT NULL,
    [Quantity] int  NOT NULL,
    [Enabled] bit  NOT NULL,
    [Thumbnail] nvarchar(max)  NOT NULL,
    [Width] float  NULL,
    [Height] float  NULL,
    [Weight] float  NULL,
    [DigitalFile] nvarchar(max)  NULL,
    [CreatedOn] datetime  NOT NULL,
    [CreatedBy] nvarchar(200)  NOT NULL,
    [SellerName] nvarchar(max)  NOT NULL,
    [Type] int  NOT NULL,
    [Currency] int  NOT NULL,
    [Tags] nvarchar(max)  NULL,
    [Images] nvarchar(max)  NULL,
    [StoreId] int  NOT NULL
);
GO
ALTER TABLE [str].[Items]
ADD CONSTRAINT [PK_Items]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO
ALTER TABLE [str].[Items]
ADD CONSTRAINT [FK_StoreItem]
    FOREIGN KEY ([StoreId])
    REFERENCES [str].[Stores]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StoreItem'
CREATE INDEX [IX_FK_StoreItem]
ON [str].[Items]
    ([StoreId]);
GO