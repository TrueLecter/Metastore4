﻿CREATE TABLE [str].[Stores] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(256)  NOT NULL UNIQUE,
    [Country] nvarchar(max)  NOT NULL,
    [Currency] int  NOT NULL
);
GO
ALTER TABLE [str].[Stores]
ADD CONSTRAINT [PK_Stores]
    PRIMARY KEY CLUSTERED ([Id] ASC);
