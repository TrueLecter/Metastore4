﻿CREATE TABLE [usr].[Wishlists]
(
    [UserId] NVARCHAR(128) NOT NULL, 
    [DateAdded] DATETIME NOT NULL, 
    [ItemId] INT NOT NULL,
    CONSTRAINT PK_Wishlist PRIMARY KEY (UserId,ItemId), 
    CONSTRAINT [FK_Wishlists_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers]([Id]),
	CONSTRAINT [FK_Wishlist_Items] FOREIGN KEY (ItemId) REFERENCES [str].[Items] (Id)
)
